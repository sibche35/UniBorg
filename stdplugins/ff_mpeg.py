"""FFMpeg for @UniBorg
dk
"""
import asyncio
import io
import os
import shutil
import re

import time
from datetime import datetime
from hachoir.metadata import extractMetadata
from hachoir.parser import createParser
from uniborg.util import admin_cmd, progress
import magic
from hurry.filesize import size


FF_MPEG_DOWN_LOAD_MEDIA_PATH = "uniborg.media.ffmpeg"


@borg.on(admin_cmd(pattern="ffmpegsave"))
async def ff_mpeg_trim_cmd(event):
    if event.fwd_from:
        return
    if not os.path.exists(FF_MPEG_DOWN_LOAD_MEDIA_PATH):
        if not os.path.isdir(Config.TMP_DOWNLOAD_DIRECTORY):
            os.makedirs(Config.TMP_DOWNLOAD_DIRECTORY)
        if event.reply_to_msg_id:
            start = datetime.now()
            reply_message = await event.get_reply_message()
            try:
                c_time = time.time()
                downloaded_file_name = await borg.download_media(
                    reply_message,
                    FF_MPEG_DOWN_LOAD_MEDIA_PATH,
                    progress_callback=lambda d, t: asyncio.get_event_loop().create_task(
                        progress(d, t, event, c_time, "trying to download")
                    )
                )
            except Exception as e:  # pylint:disable=C0103,W0703
                await event.edit(str(e))
            else:
                end = datetime.now()
                ms = (end - start).seconds
                await event.edit("Downloaded to `{}` in {} seconds.".format(downloaded_file_name, ms))
        else:
            await event.edit("Reply to a Telegram media file")
    else:
        await event.edit(f"a media file already exists in path. Please remove the media and try again!\n`.exec rm {FF_MPEG_DOWN_LOAD_MEDIA_PATH}`")


@borg.on(admin_cmd(pattern="ffmpegtrim"))
async def ff_mpeg_trim_cmd(event):
    if event.fwd_from:
        return
    if not os.path.exists(FF_MPEG_DOWN_LOAD_MEDIA_PATH):
        await event.edit(f"a media file needs to be downloaded, and saved to the following path: `{FF_MPEG_DOWN_LOAD_MEDIA_PATH}`")
        return
    current_message_text = event.raw_text
    cmt = current_message_text.split(" ")
    logger.info(cmt)
    start = datetime.now()
    if len(cmt) == 3:
        # output should be video
        cmd, start_time, end_time = cmt
        o = await cult_small_video(
            FF_MPEG_DOWN_LOAD_MEDIA_PATH,
            Config.TMP_DOWNLOAD_DIRECTORY,
            start_time,
            end_time
        )
        logger.info(o)
        try:
            c_time = time.time()
            await borg.send_file(
                event.chat_id,
                o,
                caption=" ".join(cmt[1:]),
                force_document=False,
                supports_streaming=True,
                allow_cache=False,
                # reply_to=event.message.id,
                progress_callback=lambda d, t: asyncio.get_event_loop().create_task(
                    progress(d, t, event, c_time, "trying to upload")
                )
            )
            os.remove(o)
        except Exception as e:
            logger.info(str(e))
    elif len(cmt) == 2:
        # output should be image
        cmd, start_time = cmt
        o = await take_screen_shot(
            FF_MPEG_DOWN_LOAD_MEDIA_PATH,
            Config.TMP_DOWNLOAD_DIRECTORY,
            start_time
        )
        logger.info(o)
        try:
            c_time = time.time()
            await borg.send_file(
                event.chat_id,
                o,
                caption=" ".join(cmt[1:]),
                force_document=True,
                # supports_streaming=True,
                allow_cache=False,
                # reply_to=event.message.id,
                progress_callback=lambda d, t: asyncio.get_event_loop().create_task(
                    progress(d, t, event, c_time, "trying to upload")
                )
            )
            os.remove(o)
        except Exception as e:
            logger.info(str(e))
    else:
        await event.edit("RTFM")
        return
    end = datetime.now()
    ms = (end - start).seconds
    await event.edit(f"Completed Process in {ms} seconds")


async def take_screen_shot(video_file, output_directory, ttl):
    # https://stackoverflow.com/a/13891070/4723940
    out_put_file_name = output_directory + \
        "/" + str(time.time()) + ".jpg"
    file_genertor_command = [
        "ffmpeg",
        "-ss",
        str(ttl),
        "-i",
        video_file,
        "-vframes",
        "1",
        out_put_file_name
    ]
    # width = "90"
    process = await asyncio.create_subprocess_exec(
        *file_genertor_command,
        # stdout must a pipe to be accessible as process.stdout
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
    )
    # Wait for the subprocess to finish
    stdout, stderr = await process.communicate()
    e_response = stderr.decode().strip()
    t_response = stdout.decode().strip()
    if os.path.lexists(out_put_file_name):
        return out_put_file_name
    else:
        logger.info(e_response)
        logger.info(t_response)
        return None

# https://github.com/Nekmo/telegram-upload/blob/master/telegram_upload/video.py#L26

async def cult_small_video(video_file, output_directory, start_time, end_time):
    # https://stackoverflow.com/a/13891070/4723940
    out_put_file_name = output_directory + \
        "/" + str(round(time.time())) + ".mp4"
    file_genertor_command = [
        "ffmpeg",
        "-i",
        video_file,
        "-ss",
        start_time,
        "-to",
        end_time,
        "-async",
        "1",
        "-strict",
        "-2",
        out_put_file_name
    ]
    process = await asyncio.create_subprocess_exec(
        *file_genertor_command,
        # stdout must a pipe to be accessible as process.stdout
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
    )
    # Wait for the subprocess to finish
    stdout, stderr = await process.communicate()
    e_response = stderr.decode().strip()
    t_response = stdout.decode().strip()
    if os.path.lexists(out_put_file_name):
        return out_put_file_name
    else:
        logger.info(e_response)
        logger.info(t_response)
        return None



@borg.on(admin_cmd(pattern='ff'))
async def ff_handler(event):
    current_message_text = event.raw_text
    cmt = current_message_text.split(' ')
    if event.fwd_from:
        return
    if not os.path.exists(Config.TMP_DOWNLOAD_DIRECTORY):
        os.makedirs(Config.TMP_DOWNLOAD_DIRECTORY)
    if event.reply_to_msg_id:
        rep = await event.get_reply_message()
        try:
            c_time = time.time()
            path = await borg.download_media(rep,
                       progress_callback=lambda d,t: asyncio.get_event_loop().create_task(progress(d, t, event, c_time, 'trying to download')))
        except Exception as e:
            await event.edit(str(e))
    else:
        path = cmt[1]
    input_str = ' '.join(cmt[1:])
    await event.edit('Processing...')
    try:
        c_time = time.time()
        output,caption = await ffmpeg(input_str, path,
                              Config.TMP_DOWNLOAD_DIRECTORY)
        await borg.send_file(event.chat_id, output,
                             caption=caption,
                             progress_callback=lambda d,t: asyncio.get_event_loop().create_task(progress(d, t, event, c_time, 'trying to upload')))
        #if os.path.exists(path):
            #os.remove(path)
    except Exception as e:
        await event.edit(f'Something is worong {str(e)}')



#check if space in filename bcuz ffmpeg get an error
def remove_from_file_name(file_name):
        new_file_name = ''
        for new_name in file_name:
            if new_name == ' ':
                continue
            if new_name == '(':
                continue
            if new_name == ')':
                continue
            new_file_name += new_name
        with open(file_name, 'rb')as p:
            with open(new_file_name, 'wb')as f:
                shutil.copyfileobj(p, f)
        os.remove(file_name)
        return  new_file_name


async def ffmpeg(input_str, in_path, output_directory):
    cap = False
    if ' ' in in_path:
        in_path = remove_from_file_name(in_path)
    orig_file_ext = os.path.splitext(in_path)[0]
    out_put_file_name = output_directory + str(round(time.time())) + "{}"
    out_file_name = out_put_file_name.format(orig_file_ext)
    file_genertor_command = [
        "ffmpeg",
        "-y",
        "-i",
        in_path,
        out_file_name
    ]

    # output format
    if "-format" in input_str:
        pattern = (r"-format[\s=](.{2,4})")
        match = re.search(pattern, input_str)
        file_genertor_command.pop()
        out_filename = out_put_file_name.format('.'+match.group(1))
        file_genertor_command.append(out_filename)
        if '-preset' in input_str:
            pattern = (r'-preset[\s](\w{3,10})\s(-crf\s\d{1,2})')
            match = re.search(pattern, input_str)
            if match:
                value = match.group(1).strip()
                file_genertor_command.insert(4, f"-c:v libx264")
                file_genertor_command.insert(5, f"-preset {value}")
                file_genertor_command.insert(6, match.group(2))
    else:
        output_format = os.path.splitext(in_path)[-1]
        out_file_name = out_put_file_name.format(output_format)
        file_genertor_command.pop()
        file_genertor_command.append(out_file_name)

    if '-show' in input_str:
        cap = True
    if "-vn" in input_str:  # no video
        file_genertor_command.insert(4, '-vn')
        if not file_genertor_command[-1].endswith('.mp3'):
            file_genertor_command.pop()
            out_file_name = out_put_file_name.format('.mp3')
            file_genertor_command.append(out_file_name)
    if "-low" in input_str:
        low_filter = "-acodec libmp3lame -b:a 8k -ac 1 -ar 11025"
        file_genertor_command.insert(4, low_filter)
        if not file_genertor_command[-1].endswith('.mp3'):
            p=file_genertor_command.pop()
            out_file_name = out_put_file_name.format('.mp3')
            file_genertor_command.append(out_file_name)
    if "-an" in input_str:  # no audio
        file_genertor_command.insert(4, '-an')
    if '-trible' in input_str:
        pattern = (r'-trible[\s=](-?\d{1,2})')
        match = re.search(pattern, input_str)
        trible_gain = match.group(1)
        audio_filter = f"-af \"firequalizer=gain_entry='entry(0,{trible_gain});entry(250,-11.5);entry(1000,0);entry(4000,8);entry(16000,16)'\""
        file_genertor_command.insert(4, audio_filter)
        if not file_genertor_command[-1].endswith('.mp3'):
            file_genertor_command.pop()
            out_file_name = out_put_file_name.format('.mp3')
            file_genertor_command.append(out_file_name)
    if "-resize" in input_str:
        pattern = (r'-resize[\s=](.{2,4}):(.{2,4})')
        match = re.search(pattern, input_str)
        width = match.group(1).strip()
        height = match.group(2).strip()
        video_filter =  f"-vf scale={width}:{height}"
        file_genertor_command.insert(4, video_filter)
    if "-crop" in input_str:
        pattern = (r'-crop[\s=](\d{2}?:\d{2}:\d{2}?),(\d{2}?:\d{2}:\d{2}?)')
        match = re.search(pattern, input_str)
        start = "-ss " + match.group(1)
        end = "-to " + match.group(2)
        file_genertor_command.insert(4, f"{start} {end}")
    # get the filename without extention
    #file_name = await os.path.splitext(in_path)[0]
    cmd = ' '.join(file_genertor_command)
    #caption = f"{file_name}\nsize {size_}"
    process = await asyncio.create_subprocess_shell(
        cmd,
        # stdout must a pipe to be accessible as process.stdout
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
    )
    # Wait for the subprocess to finish
    stdout, stderr = await process.communicate()
    e_response = stderr.decode().strip()
    t_response = stdout.decode().strip()
    if os.path.lexists(file_genertor_command[-1]):
        if cap:
            infostat = os.stat(file_genertor_command[-1]).st_size
            human_readble = size(infostat)
            caption = f"{orig_file_ext}\n`{cmd}`\nsize {human_readble}"
            return file_genertor_command[-1], caption
        return file_genertor_command[-1], None
    else:
        logger.info(e_response)
        logger.info(t_response)
        return None
